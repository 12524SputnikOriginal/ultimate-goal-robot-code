package org.firstinspires.ftc.teamcode.auto.routes.encoders;

import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;
import org.firstinspires.ftc.teamcode.auto.AbstractPath;

public class RedSideBEnc extends AbstractPath {
    @Override
    public void startPath(DriveTrain dt, Wobble wob, Shooter sh){
        DriveTrain mover = dt;
        setDt(dt);
        Wobble wobble  = wob;
        Shooter shooter = sh;
        wobble.move(-0.6, 500);
        wobble.wobbleStop();
        mover.move(0, 0.6, 0, 1200);
        sleep(500);
        mover.move(-0.4,0,0,200);
        sleep(200);
        mover.move(0, 0, 0.5, 600);
        sleep(500);
        mover.setPower(0,-0.4,0);
        sleep(500);
        mover.move(0, 0.5, 0, 550);
        sleep(500);
        wobble.wobbleOut();
        mover.move(0, -0.5, 0, 400);
        mover.move(0, 0, -0.5, 550);
        mover.setPower(0.4,0,0);
        sleep(1000);
        mover.move(-0.4, 0, 0, 100);
        mover.move(0, -0.5, 0, 650);
        wobble.wobbleMid();
        sleep(1000);
    }
}