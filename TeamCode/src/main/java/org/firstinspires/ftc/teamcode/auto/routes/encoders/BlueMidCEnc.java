package org.firstinspires.ftc.teamcode.auto.routes.encoders;

import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;
import org.firstinspires.ftc.teamcode.auto.AbstractPath;

public class BlueMidCEnc extends AbstractPath {
    @Override
    public void startPath(DriveTrain dt, Wobble wob, Shooter sh)
    {
       DriveTrain mover=dt;
       setDt(dt);
       Wobble wobble=wob;
       wobble.move(-0.6,500);
       wobble.wobbleStop();
       sleep(500);
       mover.move(0,0.7,0,1000);
       mover.move( 0, 0, 0.5, 540);
       mover.setPower(0.4,0,0);
       sleep(1000);
       mover.move(0,0.7,0,800);
       wobble.move(0.6,500);
       wobble.wobbleOut();
       sleep(500);
       mover.move(0,-0.7,0,1400);
       mover.move(-0.5,0,0,120);
       mover.move(0,0,-0.5,540);
       mover.move(0,-0.5,0,2000);
       sleep(500);
    }
}
