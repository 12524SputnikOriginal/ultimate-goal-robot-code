package org.firstinspires.ftc.teamcode.TeleOp;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

@TeleOp
@Disabled
public class AngularVelocity extends LinearOpMode {
    DcMotor shooter;
    public int getAngularVelocity(){
        ElapsedTime timer = new ElapsedTime();
        int k = shooter.getCurrentPosition();
        timer.reset();
        while (timer.milliseconds()<10){}
        return (shooter.getCurrentPosition()-k)*100/28;
    }

    public void runOpMode(){
        shooter = hardwareMap.dcMotor.get("shooter");
        waitForStart();
        while (opModeIsActive()) {
            shooter.setPower(-gamepad1.left_stick_y);
            telemetry.addData("power", gamepad1.left_stick_y);
            telemetry.addData("angular velocity",getAngularVelocity());
            telemetry.update();
        }
    }
}
