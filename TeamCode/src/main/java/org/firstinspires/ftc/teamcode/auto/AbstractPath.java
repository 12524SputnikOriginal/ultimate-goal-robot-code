package org.firstinspires.ftc.teamcode.auto;


import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;

public abstract class AbstractPath {
    DriveTrain dt;
    public final void setDt(DriveTrain dt ){
        this.dt=dt;
    }
    public abstract void startPath(DriveTrain dt, Wobble wob, Shooter sh);
    public final void sleep(int milliseconds){
        ElapsedTime timer = new ElapsedTime();

        while(timer.milliseconds()<milliseconds && dt.opMode.opModeIsActive()){}
    }
}
