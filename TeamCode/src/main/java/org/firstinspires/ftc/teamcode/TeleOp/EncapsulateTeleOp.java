package org.firstinspires.ftc.teamcode.TeleOp;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
        import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
        import com.qualcomm.robotcore.hardware.DigitalChannel;

        import org.firstinspires.ftc.teamcode.modules.DriveTrain;
        import org.firstinspires.ftc.teamcode.modules.Intake;
        import org.firstinspires.ftc.teamcode.modules.Shooter;
        import org.firstinspires.ftc.teamcode.modules.Wobble;

@TeleOp
public class EncapsulateTeleOp extends LinearOpMode {
    public void runOpMode() {
        DriveTrain driveTrain = new DriveTrain(this);
        Shooter shooter = new Shooter(this);
        Intake intake = new Intake(this);
        Wobble wobble = new Wobble(this);
        telemetry.addLine("I m active");
        int wobbleLift = -10;
        float rotate = 0;
        double PowerCoeF = 1;
        boolean wobbleSwitch = false;
        boolean gamepad2RightBumperFlag = false;
        boolean gamepad2DpadDownFlag = false;
        boolean gamepad2DpadUpFlag = false;
        boolean gamepad2BFlag = false;
        boolean shooterSwitch = false;
        boolean PowerFlag = false;
        DigitalChannel limitSwitch = hardwareMap.digitalChannel.get("limitSwitch");
        waitForStart();

        while (opModeIsActive()) {
            if (gamepad1.y && !PowerFlag) {
                if (PowerCoeF == 1) {
                    PowerCoeF = 0.55;
                }
                else PowerCoeF = 1;
            }
            PowerFlag = gamepad1.y;
            if (gamepad1.left_stick_y != 0 || gamepad1.left_stick_x != 0 || gamepad1.left_trigger != 0 || gamepad1.right_trigger != 0) {
                if (gamepad1.left_trigger != 0) {
                    rotate = gamepad1.left_trigger;
                }
                else {
                    rotate = -gamepad1.right_trigger;
                }
                driveTrain.setPower(gamepad1.left_stick_x*PowerCoeF, -gamepad1.left_stick_y*PowerCoeF, rotate*PowerCoeF);
            }
            else {
                driveTrain.setPower(0, 0, 0);
            }
            if (gamepad2.right_trigger != 0 && wobble.lift.getCurrentPosition() - wobbleLift >= -2500) {
                wobble.lift.setPower(-gamepad2.right_trigger);
            }
            else {
                if (gamepad2.left_trigger != 0 && limitSwitch.getState()) {
                    wobble.lift.setPower(gamepad2.left_trigger);
                }
                else {
                    wobble.lift.setPower(0);
                }
            }
            if (limitSwitch.getState() == false && wobbleSwitch == false) {
                wobbleLift = wobble.lift.getCurrentPosition();
                wobbleSwitch = true;
            }
            if (limitSwitch.getState() == false && wobbleSwitch != false) {
                wobble.lift.setPower((wobble.lift.getCurrentPosition() - wobbleLift) * (-0.002));
            }
            if (gamepad1.x) {
                wobble.wobbleOut();
                telemetry.addLine("I m free");
                telemetry.update();
            }
            if (gamepad1.b) {
                wobble.wobbleIn();
                telemetry.addLine("I m first");
                telemetry.update();
            }
            if (gamepad2.a) {
                shooter.shooterPush.setPosition(0.8);
                sleep(200);
                shooter.shooterPush.setPosition(0.28);
            }
            if (gamepad2.right_bumper && !gamepad2RightBumperFlag) {
                if (shooter.shooter.getPower() == 0) {
                    shooterSwitch = true;
                }
                else {
                    shooter.shooter.setPower(0);
                    shooterSwitch = false;
                }
            }
            else {
                if (gamepad2.left_bumper) {
                    shooter.setLowAngularVelocity();
                }
            }
            if (shooterSwitch) {
                shooter.setAngularVelocity();
            }
            else {
                if (!gamepad2.left_bumper) {
                    shooter.shooter.setPower(0);
                }
            }
            if (gamepad2.right_stick_y > 0) {
                intake.intake.setPower(1);
            }
            else {
                if (gamepad2.right_stick_y < 0) {
                    intake.intake.setPower(-1);
                }
                else intake.stop();
            }
            if (gamepad2.dpad_up && shooter.shooterAngleLeft.getPosition() < 0.54 && !gamepad2DpadUpFlag) {
                shooter.shooterAngleLeft.setPosition(shooter.shooterAngleLeft.getPosition() + 0.01);
                shooter.shooterAngleRight.setPosition(shooter.shooterAngleRight.getPosition() + 0.01);
                sleep(50);
            }

            if (gamepad2.dpad_down && shooter.shooterAngleLeft.getPosition() > 0.35 && !gamepad2DpadDownFlag) {
                shooter.shooterAngleLeft.setPosition(shooter.shooterAngleLeft.getPosition() - 0.01);
                shooter.shooterAngleRight.setPosition(shooter.shooterAngleRight.getPosition() - 0.01);
                sleep(50);
            }
            if (gamepad2.dpad_right) {
                shooter.shooterAngleLeft.setPosition(0.46);
                shooter.shooterAngleRight.setPosition(0.46);
            }
            if (gamepad2.dpad_left) {
                shooter.shooterAngleLeft.setPosition(0.45);
                shooter.shooterAngleRight.setPosition(0.45);
            }
            if (gamepad2.y) {
                shooter.shooterAngleLeft.setPosition(0.29);
                shooter.shooterAngleRight.setPosition(0.29);
                intake.intake();
            }
            else {
                if (gamepad2.x) {
                    shooter.shooterAngleLeft.setPosition(0.36);
                    shooter.shooterAngleRight.setPosition(0.36);
                    intake.throwOut();
                }
                else {
                    intake.stop();
                }
            }
            if (gamepad2.b && !gamepad2BFlag) {
                //intake.captureOpenerRight.setPower(-0.3);
                shooter.shoot();
                shooter.shoot();
                shooter.shoot();
                shooter.shoot();
            }
            else {
                //intake.captureOpenerRight.setPower(0);
            }
            gamepad2BFlag = gamepad2.b;
            if (gamepad1.right_bumper) {
                driveTrain.move(0, 0, -0.5, 50);
            }
            if (gamepad1.left_bumper) {
                driveTrain.move(0, 0, 0.5, 50);
            }
            if (gamepad1.left_bumper) {
                driveTrain.move(0, 0, 0.5, 50);
            }
            if (gamepad1.right_bumper) {
                driveTrain.move(0, 0, -0.5, 50);
            }
            gamepad2RightBumperFlag = gamepad2.right_bumper;
            gamepad2DpadUpFlag = gamepad2.dpad_up;
            gamepad2DpadDownFlag = gamepad2.dpad_down;
            gamepad2BFlag = gamepad2.b;
            telemetry.addData("k1", wobbleSwitch);
            telemetry.addData("Power:", PowerCoeF);
            telemetry.addData("v", wobbleLift);
            telemetry.addData("m1", intake.ringlift.getCurrentPosition());
            telemetry.addData("shooterAngle", shooter.shooterAngleLeft.getPosition());
            //telemetry.addData("h4",n4.getPosition());
            telemetry.update();
        }
    }
}