package org.firstinspires.ftc.teamcode.modules;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;

public class Wobble {
    Servo leftHand;
    Servo rightHand;
    public DcMotor lift;
    LinearOpMode opMode;
    DigitalChannel limitSwitch;
    public Wobble() {
    }

    public Wobble(LinearOpMode programm) {
        opMode = programm;
        init(opMode.hardwareMap);
    }

    public void init(HardwareMap hWMap) {
        leftHand = hWMap.servo.get("leftHand");
        rightHand = hWMap.servo.get("rightHand");
        lift = hWMap.dcMotor.get("lift");
        wobbleOut();
        limitSwitch=hWMap.digitalChannel.get("limitSwitch");
    }

    public void wobbleIn() {
        leftHand.setPosition(0.7);
        rightHand.setPosition(0.27);
    }

    public void wobbleOut() {
        leftHand.setPosition(0.30);
        rightHand.setPosition(0.66);
    }

    public void wobbleMid() {
        leftHand.setPosition(0.5);
        rightHand.setPosition(0.47);
    }
    public void wobbleKrivo(){
        leftHand.setPosition(0.69);
        rightHand.setPosition(0.32);
    }

    public void wobbleUp() {
        lift.setPower(-0.5);
    }

    public void wobbleDown() {
        lift.setPower(0.5);
    }

    public void wobbleStop() {
        lift.setPower(0);
    }
    public boolean isBusy() {
        boolean busys=lift.isBusy();
        boolean powers=true;
        return busys&&powers;
    }

    public void move ( double z, int tick) {
        tick = Math.abs(tick);
        lift.setTargetPosition(lift.getCurrentPosition()+tick * (int) (z/ Math.abs(z)));
        lift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        lift.setPower(z);
        while (isBusy() && opMode.opModeIsActive()){}
        lift.setPower(0);
        lift.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
    }
    }



