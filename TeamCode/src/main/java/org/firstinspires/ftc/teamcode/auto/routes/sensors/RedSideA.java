package org.firstinspires.ftc.teamcode.auto.routes.sensors;

import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;
import org.firstinspires.ftc.teamcode.auto.AbstractPath;

public class RedSideA extends AbstractPath {
    @Override
    public void startPath(DriveTrain dt, Wobble wob, Shooter sh)
    {
        DriveTrain mover = dt;
        setDt(dt);
        Wobble wobble = wob;
        wobble.wobbleIn();
        mover.setPower(0.4, 0, 0);
        sleep(500);
        mover.move(0,0.5,0,300);
        wobble.wobbleOut();
        sleep(300);
        mover.setPower(0.4, 0, 0);
        sleep(500);
        //mover.setPower(0, 0.35, 0);
        //mover.waitForWhiteLine();
        //mover.move(0, -0.3, 0,230);
        //wob.move(0.6,500);
        //sleep(200);
        //wobble.wobbleMid();
        //sleep(500);
        //mover.move(0,-0.2,0,50);
        //sleep(600);
        mover.move(0, -0.7, 0, 1800);
        mover.setPower(0,-0.4,0);
        sleep(800);
        mover.setPower(0,0,0.5);
        sleep(1000);
        mover.setPower(-0.5,0,0);
        sleep(500);
        mover.setPower(0, 0.35, 0);
        mover.waitForColoredLine();
        sleep(300);
        mover.waitForColoredLine();
        mover.move(0, -0.5, 0,300);
        mover.move(0, 0, -0.56,300);
        mover.move(0,0.7,0,200);
        wobble.wobbleIn();
        sleep(400);
        wobble.lift.setTargetPosition(-1100);
        wobble.lift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        wobble.lift.setPower(-0.5);
        //wob.move(-0.8,500);
        //dt.move(0,0.7,0,80);
        mover.move(0, 0, -0.3,145);
        mover.setPower(0.5,0,0);
        sleep(2000);
        mover.setPower(0, 0.35, 0);
        mover.waitForColoredLine();
        mover.move(0, -0.4, 0,145);
        wobble.lift.setTargetPosition(0);
        wobble.lift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        wobble.lift.setPower(0.5);
        wobble.wobbleOut();
        sleep(500);
        mover.move(-1,0,0,1000);
        mover.move(0,1,0,700);
    }
}
