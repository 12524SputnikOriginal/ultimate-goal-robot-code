package org.firstinspires.ftc.teamcode.auto;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.auto.routes.encoders.BlueSideAEnc;
import org.firstinspires.ftc.teamcode.auto.routes.encoders.BlueSideBEnc;
import org.firstinspires.ftc.teamcode.auto.routes.encoders.BlueSideCEnc;
import org.firstinspires.ftc.teamcode.auto.routes.sensors.BlueSideA;

import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.FOUR;
import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.ONE;
import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.ZERO;
@Autonomous
public class BlueSide extends AutoParent {
    @Override
    public void runOpMode() {
        initer(this);
        waitForStart();
        AbstractPath way=new BlueSideA();
        if (recognition.getAnalysis()==ZERO) {
            way=new BlueSideAEnc();
            telemetry.addLine("zone A");
            telemetry.update();
        }
        if (recognition.getAnalysis()==ONE){
            way=new BlueSideBEnc();
            telemetry.addLine("zone B");
            telemetry.update();
        }
        if (recognition.getAnalysis()==FOUR) {
            way = new BlueSideCEnc();
            telemetry.addLine("zone C");
            telemetry.update();
        }
        wob.wobbleIn();
        sleep(200);
        wob.move(-0.6, 600);
        dt.setPower(-0.5, 0, 0);
        sleep(1000);
        dt.move(0, 0.7, 0, 1300);
        wob.move(0.6, 500);
        wob.wobbleStop();
        dt.setPower(-0.5, 0, 0);
        sleep(300);
        dt.move(0, 0, 0.5, 330);
        sleep(1000);
        sh.setAngle(0.54);
        sh.shoot();
        sh.shoot();
        telemetry.addData("shooter power", sh.shooter.getPower());
        telemetry.update();
        sh.shoot();
        sh.stopShooter();
        dt.setPower(-0.5, 0, 0);
        sleep(300);
        dt.setPower(0, 0, 0);
        sleep(300);
        way.startPath(dt,wob,sh);
    }
}