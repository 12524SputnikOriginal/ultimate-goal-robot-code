package org.firstinspires.ftc.teamcode.auto.routes.sensors;

import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;
import org.firstinspires.ftc.teamcode.auto.AbstractPath;

public class RedMidA extends AbstractPath {
    @Override
    public void startPath(DriveTrain dt, Wobble wob, Shooter sh)
    {
        DriveTrain mover = dt;
        setDt(dt);
        Wobble wobble = wob;
        mover.setPower(0, 0.35, 0);
        mover.waitForWhiteLine();
        mover.move(0,0.35,0,50);
        mover.move(0, 0, -0.3, 700);
        mover.setPower(0, 0.3, 0);
        mover.waitForColoredLine();
        mover.setPower(0,-0.4,0);
        sleep(50);
        wobble.wobbleOut();
        mover.move(0, -0.4, 0, 500);
    }
}