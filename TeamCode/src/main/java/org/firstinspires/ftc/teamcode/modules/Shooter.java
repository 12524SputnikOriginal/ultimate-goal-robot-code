package org.firstinspires.ftc.teamcode.modules;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.PIDFCoefficients;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

public class Shooter {
    public DcMotorEx shooter;
    public Servo shooterAngleLeft;
    public Servo shooterPush;
    public Servo shooterAngleRight;
    LinearOpMode opMode;
    public final double angularVelocity=-1100;
    private AngularVelocityController angularVelocityController;

    public Shooter(){}
    public Shooter(LinearOpMode programm){
        opMode=programm;
        init(opMode.hardwareMap);
    }

    public void setAngularVelocity(){
        shooter.setVelocity(angularVelocity);
    }

    public void setAngularVelocity(int vel){
        shooter.setVelocity(vel);
    }

    public void setLowAngularVelocity(){ shooter.setVelocity(-500);}

    public void init(HardwareMap hWMap){
        shooter = (DcMotorEx)hWMap.dcMotor.get("shooter");
        shooter.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        shooter.setPIDFCoefficients(DcMotor.RunMode.RUN_USING_ENCODER,new PIDFCoefficients(0,0,0,24)); //60,0.035,4,14.82//
        shooterAngleLeft = hWMap.servo.get("shooterAngleLeft");
        shooterAngleRight = hWMap.servo.get("shooterAngleRight");
        shooterPush = hWMap.servo.get("shooterPush");
        shooterAngleLeft.setPosition(0.44);
        shooterPush.setPosition(0.28);
        shooterAngleRight.setDirection(Servo.Direction.REVERSE);
    }

    public double getAngularVelocity(){
        return shooter.getVelocity();
    }

    public void stopShooter(){
        shooter.setPower(0);
    }
    public void shoot(){
        ElapsedTime timer = new ElapsedTime();
        setAngularVelocity();
        while (Math.abs(shooter.getVelocity())<Math.abs(angularVelocity)+300){
            opMode.telemetry.addData("powerofwhile1",getAngularVelocity());
            opMode.telemetry.update();
        }
        shooterPush.setPosition(0.6);
        timer.reset();
        while (timer.milliseconds()<300&&opMode.opModeIsActive()){}
        shooterPush.setPosition(0.25);
        timer.reset();
        while (timer.milliseconds()<300){}
        opMode.telemetry.addData("powerofwhile23",getAngularVelocity());
        opMode.telemetry.update();
    }
    public void onlyShoot(){
        ElapsedTime timer = new ElapsedTime();
        shooterPush.setPosition(0.6);
        timer.reset();
        while (timer.milliseconds()<600&&opMode.opModeIsActive()){}
        shooterPush.setPosition(0.25);
    }

    public void setAngle(double Angle)
    {
        shooterAngleLeft.setPosition(Angle*0.19+0.35);
        shooterAngleRight.setPosition(Angle*0.19+0.35);
    }

    public double getAngle(){
        double Position = (shooterAngleLeft.getPosition() - 0.35)/0.19;
        return Position;
    }

    class AngularVelocityController extends Thread{
        public void run(){
            while (!isInterrupted())
            {
                if (getAngularVelocity() < angularVelocity) {
                    shooter.setPower(1);
                }
                if (getAngularVelocity() > angularVelocity){
                    shooter.setPower(0);
                }
            }
        }
    }
}
