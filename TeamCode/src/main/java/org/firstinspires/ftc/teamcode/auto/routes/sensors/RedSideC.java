package org.firstinspires.ftc.teamcode.auto.routes.sensors;

import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;
import org.firstinspires.ftc.teamcode.auto.AbstractPath;

public class RedSideC extends AbstractPath {
    @Override
    public void startPath(DriveTrain dt, Wobble wob, Shooter sh){
        DriveTrain mover = dt;
        setDt(dt);
        Wobble wobble  = wob;
        Shooter shooter = sh;
        wobble.wobbleIn();
        mover.move(0,1,0,1650);
        mover.setPower(0,0.35,0);
        mover.waitForColoredLine();
        //mover.setPower(0,0,0);
        wobble.wobbleOut();
        sleep(300);
        mover.setPower(0.4, 0, 0);
        sleep(500);
        mover.move(0,-1,0,700);
        mover.setPower(0,-0.35,0);
        mover.waitForWhiteLine();

        //mover.setPower(0,0,0);
        //wobble.wobbleMid();
        //sleep(1000);
    }
}
