package org.firstinspires.ftc.teamcode.modules;

        import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
        import com.qualcomm.robotcore.hardware.CRServo;
        import com.qualcomm.robotcore.hardware.DcMotor;
        import com.qualcomm.robotcore.hardware.HardwareMap;
        import com.qualcomm.robotcore.hardware.Servo;

public class Intake {
    public DcMotor ringlift;
    public DcMotor intake;
    Servo captureOpenerLeft;
    public CRServo captureOpenerRight;
    LinearOpMode opMode;


    public Intake(){}
    public Intake(LinearOpMode programm) {
        opMode = programm;
        init(opMode.hardwareMap);
    }
    public void init(HardwareMap hWMap){
        intake = hWMap.dcMotor.get("intake");
        ringlift = hWMap.dcMotor.get("ringlift");
        captureOpenerRight = hWMap.crservo.get("captureOpenerRight");
        captureOpenerLeft = hWMap.servo.get("captureOpenerLeft");
        //captureOpenerRight.setPosition(0.72);
        captureOpenerLeft.setPosition(0.71);
    }
    public void intake() {
        intake.setPower(1);
        ringlift.setPower(-1);
    }
    public void throwOut(){
        intake.setPower(-1);
        ringlift.setPower(1);
    }
    public void stop(){
        intake.setPower(0);
        ringlift.setPower(0);
    }
    public void intakeDown(){
        captureOpenerLeft.setPosition(0.72);
        captureOpenerRight.setPower(0.3);
    }
    public void intakeUp(){
        captureOpenerLeft.setPosition(0);
        captureOpenerRight.setPower(-0.3);
    }
}
