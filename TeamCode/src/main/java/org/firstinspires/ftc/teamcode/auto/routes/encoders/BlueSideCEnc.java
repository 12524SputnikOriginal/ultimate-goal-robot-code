package org.firstinspires.ftc.teamcode.auto.routes.encoders;

import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;
import org.firstinspires.ftc.teamcode.auto.AbstractPath;

public class BlueSideCEnc extends AbstractPath {
    @Override
    public void startPath(DriveTrain dt, Wobble wob, Shooter sh) {
        DriveTrain mover = dt;
        setDt(dt);
        Wobble wobble  = wob;
        Shooter shooter = sh;
        wobble.wobbleIn();
        sleep(1000);
        wobble.move(-0.6, 500);
        wobble.wobbleStop();
        mover.setPower(-0.5, 0, 0);
        sleep(2000);
        mover.move(0, 0.6, 0, 4500);
        mover.setPower(-0.4,0,0);
        sleep(1000);
        wobble.wobbleOut();
        mover.move(0, -0.6, 0, 1240);
        sleep(500);
        wobble.wobbleIn();
        sleep(1000);
    }
}


