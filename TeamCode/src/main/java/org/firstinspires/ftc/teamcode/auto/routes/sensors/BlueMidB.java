package org.firstinspires.ftc.teamcode.auto.routes.sensors;

import org.firstinspires.ftc.teamcode.auto.AbstractPath;
import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;

public class BlueMidB extends AbstractPath {
    @Override
    public void startPath(DriveTrain dt, Wobble wob, Shooter sh) {
        DriveTrain mover = dt;
        setDt(dt);
        Wobble wobble = wob;
        mover.move(0.5, 0, 0, 400);
        mover.move(0, 0.5, 0, 1500);
        mover.distanceSensorMovement(0.6,10);
        mover.move(0, 0, 0.3, 500);
        wobble.wobbleOut();
        sleep(500);
        mover.move(0, -0.5, 0, 550);
        sleep(100);
        mover.move(0, 0, -0.3, 500);
        mover.setPower(0, -0.35, 0);
        mover.waitForWhiteLine();
        mover.move(0,0.35,0,150);
        mover.setPower(0, 0, 0);
        wobble.wobbleIn();
    }
}