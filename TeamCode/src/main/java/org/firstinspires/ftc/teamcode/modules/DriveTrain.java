package org.firstinspires.ftc.teamcode.modules;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.firstinspires.ftc.teamcode.modules.recognition.SputnikColorSensor;

import static org.firstinspires.ftc.teamcode.modules.recognition.SputnikColorSensor.LineColor.BLACK;
public class DriveTrain {
    private DcMotor leftForward, rightForward, leftBack, rightBack;
    public  DistanceSensor sensorRange;
    SputnikColorSensor colorSensor = new SputnikColorSensor();
    public LinearOpMode opMode;
    ElapsedTime timer = new ElapsedTime();

    public DriveTrain(LinearOpMode programm){
        opMode=programm;
        init(opMode.hardwareMap);
    }
    
    public void init (HardwareMap hwMap){
        leftForward=hwMap.get(DcMotor.class,"leftForward");
        rightForward=hwMap.get(DcMotor.class,"rightForward");
        leftBack=hwMap.get(DcMotor.class,"leftBack");
        rightBack=hwMap.get(DcMotor.class,"rightBack");
        sensorRange=hwMap.get(DistanceSensor.class, "sensor_range");
        colorSensor.init( hwMap);
    }

    public void setMode(DcMotor.RunMode dcMode){
        leftForward.setMode(dcMode);
        rightForward.setMode(dcMode);
        leftBack.setMode(dcMode);
        rightBack.setMode(dcMode);
    }

    public void setPower (double x, double y, double z ) {
        leftForward.setPower(x-y+z);
        rightForward.setPower(x+y+z);
        leftBack.setPower(-x-y+z);
        rightBack.setPower(-x+y+z);
    }

    public void move (double x, double y, double z, int tick) {
        setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        tick = Math.abs(tick);
        double leftForwardPower, rightForwardPower, leftBackPower, rightBackPower;
        leftForwardPower = x - y + z;
        rightForwardPower = x + y + z;
        leftBackPower = -x - y + z;
        rightBackPower = -x + y + z;
        leftForward.setTargetPosition(tick * (int) (leftForwardPower / Math.abs(leftForwardPower)));
        rightForward.setTargetPosition(tick * (int) (rightForwardPower / Math.abs(rightForwardPower)));
        leftBack.setTargetPosition(tick * (int) (leftBackPower / Math.abs(leftBackPower)));
        rightBack.setTargetPosition(tick * (int) (rightBackPower / Math.abs(rightBackPower)));
        setMode(DcMotor.RunMode.RUN_TO_POSITION);
        setPower( x, y,  z);
        while(isBusy()&&opMode.opModeIsActive()){}
        setPower(0,0,0);
        setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
    }

    public boolean isBusy() {
        boolean busys=leftBack.isBusy()&&leftForward.isBusy()&&rightBack.isBusy()&&rightForward.isBusy();
        boolean powers=(Math.abs(leftBack.getPower())+Math.abs(rightBack.getPower())+Math.abs(leftForward.getPower())+Math.abs(rightForward.getPower()))>0.05;
        return busys&&powers;
    }

    public void distanceSensorMovement (double y, double cm) {
        ElapsedTime timer=new ElapsedTime();
        double firstPosition =sensorRange.getDistance(DistanceUnit.CM);
        if(Math.abs(y)>0.2) {
            while (opMode.opModeIsActive()&&Math.abs(firstPosition - sensorRange.getDistance(DistanceUnit.CM)) < cm - 30) {
                setPower(0, y, 0);
            }
            timer.reset();
            while (opMode.opModeIsActive()&&timer.milliseconds() < 100) {
                setPower(0, -0.2 * (y / Math.abs(y)), 0);
            }
        }
        while (opMode.opModeIsActive()&&Math.abs(firstPosition-sensorRange.getDistance(DistanceUnit.CM)) <cm) {
            setPower(0,0.2*(y/Math.abs(y)), 0);
            opMode.telemetry.addData("distance", sensorRange.getDistance(DistanceUnit.CM));
            opMode.telemetry.update();
        }
        setPower(0,0,0);
        opMode.sleep(100);
    }

    public void waitForWhiteLine() {
        timer.reset();
        while (colorSensor.getWhiteLine()==BLACK&&opMode.opModeIsActive()){
            if (timer.milliseconds()<3000){}
            else{
                break;
            }
        }
    }
    public void waitForColoredLine() {
        timer.reset();
        while (colorSensor.getCoroledLine()==BLACK&&opMode.opModeIsActive()){
            opMode.telemetry.addData("hue", colorSensor.getHsv()[0]);
            opMode.telemetry.addData("saturation", colorSensor.getHsv()[1]);
            opMode.telemetry.addData("value", colorSensor.getHsv()[2]);
            opMode.telemetry.update();

            if (timer.milliseconds()<3000){}
            else{
                break;
            }
        }
    }
    public void waitForHighSpeedLine(){
        timer.reset();
        while (!colorSensor.getHighSpeedLine()&&opMode.opModeIsActive()){
            opMode.telemetry.addData("",colorSensor.getHsv()[0]+colorSensor.getHsv()[1]+colorSensor.getHsv()[2]);
            opMode.telemetry.update();
            if(timer.milliseconds()<5000){}
            else{
                break;
            }
        }
    }
}