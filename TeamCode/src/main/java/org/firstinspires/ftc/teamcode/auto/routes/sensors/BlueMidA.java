package org.firstinspires.ftc.teamcode.auto.routes.sensors;

import org.firstinspires.ftc.teamcode.auto.AbstractPath;
import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;

public class BlueMidA extends AbstractPath {
    @Override
    public void startPath(DriveTrain dt, Wobble wob, Shooter sh)
    {
        DriveTrain mover = dt;
        setDt(dt);
        Wobble wobble = wob;
        mover.setPower(0, 0.35, 0);
        mover.waitForWhiteLine();
        mover.move(0, 0, 0.3, 555);
        mover.setPower(0, 0.35 , 0);
        mover.waitForColoredLine();
        mover.setPower(0, -0.4, 0);
        sleep(50);
        wobble.wobbleOut();
        mover.move(0, -0.8, 0, 1000);
    }
}