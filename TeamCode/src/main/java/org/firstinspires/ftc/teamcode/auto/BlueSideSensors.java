package org.firstinspires.ftc.teamcode.auto;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.PIDCoefficients;
import com.qualcomm.robotcore.hardware.PIDFCoefficients;

import org.firstinspires.ftc.teamcode.auto.routes.sensors.BlueSideA;
import org.firstinspires.ftc.teamcode.auto.routes.sensors.BlueSideB;
import org.firstinspires.ftc.teamcode.auto.routes.sensors.BlueSideC;
import org.firstinspires.ftc.teamcode.auto.routes.sensors.RedSideA;
import org.firstinspires.ftc.teamcode.auto.routes.sensors.RedSideB;
import org.firstinspires.ftc.teamcode.auto.routes.sensors.RedSideC;

import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.FOUR;
import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.ONE;
import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.ZERO;
@Autonomous
public class BlueSideSensors extends AutoParent {
    @Override
    public void runOpMode(){
        initer(this);
        waitForStart();
        AbstractPath way=new BlueSideB();
        if (recognition.getAnalysis()==ZERO) {
            sleep(8000);
            way=new BlueSideA();
            telemetry.addLine("zone A");
            telemetry.update();
        }
        if (recognition.getAnalysis()==ONE){
            way=new BlueSideB();
            telemetry.addLine("zone B");
            telemetry.update();
        }
        if (recognition.getAnalysis()==FOUR) {
            way = new BlueSideC();
            telemetry.addLine("zone C");
            telemetry.update();
        }
        wob.wobbleIn();
        sleep(500);
        wob.move(-0.8,800);
        dt.setPower(-0.5, 0, 0);
        sleep(2200);
        dt.setPower(0, 0.35, 0);
        dt.waitForColoredLine();
        dt.move(0, -0.4, 0,150);
        wob.move(0.8, 800);
        sleep(200);
        dt.setPower(-0.5, 0, 0);
        sleep(500);
        dt.move(0.5, 0, 0, 55);
        dt.move(0, 0, -0.3, 350);
        sh.setAngle(0.52);
        sh.setAngularVelocity();
        sleep(2000);
        sh.shoot();
        //sh.setAngle(sh.getAngle()+0.007);
        sh.shoot();
        //sh.setAngle(sh.getAngle()+0.007);
        telemetry.addData("shooter power", sh.getAngularVelocity());
        telemetry.update();
        sh.shoot();
        wob.move(-0.8,500);
        sh.stopShooter();
        dt.setPower(-0.5, 0, 0);
        sleep(300);
        way.startPath(dt, wob, sh);
    }
}
