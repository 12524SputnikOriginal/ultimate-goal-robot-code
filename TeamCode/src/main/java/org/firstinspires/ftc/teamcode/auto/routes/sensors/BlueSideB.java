package org.firstinspires.ftc.teamcode.auto.routes.sensors;

import org.firstinspires.ftc.teamcode.auto.AbstractPath;
import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;

public class BlueSideB extends AbstractPath {
    @Override
    public void startPath(DriveTrain dt, Wobble wob, Shooter sh){
        DriveTrain mover = dt;
        setDt(dt);
        Wobble wobble  = wob;
        Shooter shooter = sh;
        wobble.wobbleIn();
        sleep(300);
        mover.setPower(-0.4, 0, 0);
        sleep(800);
        mover.setPower(0, 0.4, 0);
        mover.waitForWhiteLine();
        mover.waitForColoredLine();
        mover.move(0,0.3,0,800);
        mover.move(0.4,0,0,270);
        mover.move(0, 0, -0.3, 540);
        mover.setPower(0, -0.4, 0);
        sleep(900);
        mover.setPower(0,0.4,0);
        mover.waitForColoredLine();
        mover.move(0,-0.4,0,50);
        mover.setPower(0,0,0);
        wob.move(0.6,500);
        mover.move(0, 0.4, 0, 50);
        wobble.wobbleDown();
        sleep(150);
        wobble.wobbleStop();
        wobble.wobbleOut();
        sleep(300);
        mover.setPower(0, -0.4, 0);
        mover.waitForColoredLine();
        mover.move(0, 0.4, 0, 50);//противовес
        mover.move(0, 0, 0.3, 500);
        mover.setPower(-0.4,0,0);
        sleep(800);
        mover.setPower(0, -0.4, 0);
        mover.waitForWhiteLine();
        mover.move(0, 0.4, 0,70);
        wobble.wobbleMid();
        sleep(400);
    }
}
