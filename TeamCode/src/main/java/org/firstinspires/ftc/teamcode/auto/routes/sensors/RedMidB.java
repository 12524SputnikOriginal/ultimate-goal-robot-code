package org.firstinspires.ftc.teamcode.auto.routes.sensors;

import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;
import org.firstinspires.ftc.teamcode.auto.AbstractPath;

public class RedMidB extends AbstractPath {
    @Override
    public void startPath(DriveTrain dt, Wobble wob, Shooter sh)
    {
        DriveTrain mover = dt;
        setDt(dt);
        Wobble wobble = wob;
        mover.move(0,0.5,0,1400);
        mover.distanceSensorMovement(0.6,15);
        mover.move(0, 0, -0.3, 530);
        mover.move(0,0.3,0,250);
        wobble.wobbleOut();
        sleep(500);
        mover.move(0, -0.5, 0, 550);
        sleep(100);
        mover.move(0, 0, 0.3, 360);
        mover.setPower(0, -0.35, 0);
        mover.waitForWhiteLine();
        mover.move(0,0.35,0,150);
        mover.setPower(0, 0, 0);
        wobble.wobbleIn();
    }
}