package org.firstinspires.ftc.teamcode.auto;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.auto.routes.encoders.BlueMidAEnc;
import org.firstinspires.ftc.teamcode.auto.routes.encoders.BlueMidBEnc;
import org.firstinspires.ftc.teamcode.auto.routes.encoders.BlueMidCEnc;
import org.firstinspires.ftc.teamcode.auto.routes.encoders.BlueSideAEnc;

import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.FOUR;
import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.ONE;
import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.ZERO;

@Autonomous
public class BlueMid extends AutoParent {
    @Override
    public void runOpMode() {
        initer(this);
        waitForStart();
        AbstractPath way=new BlueMidAEnc();
        if (recognition.getAnalysis()==ZERO) {
            way=new BlueSideAEnc();
            telemetry.addLine("zone A");
            telemetry.update();
        }
        if (recognition.getAnalysis()==ONE){
            way=new BlueMidBEnc();
            telemetry.addLine("zone B");
            telemetry.update();
        }
        if (recognition.getAnalysis()==FOUR) {
            way = new BlueMidCEnc();
            telemetry.addLine("zone C");
            telemetry.update();
        }
        wob.wobbleIn();
        wob.move(-0.6,500);
        wob.wobbleStop();
        dt.move(0,0.7,0,1300);
        wob.move(0.6,500);
        wob.wobbleStop();
        dt.move(0,0,0.3,50);
        sh.setAngle(0.6);
        sh.shoot();
        dt.move(0,0,0.5,50);
        sh.shoot();
        dt.move(0,0,0.5,50);
        sh.shoot();
        dt.move(0,0,-0.3,150);
        way.startPath(dt,wob,sh);
    }
}
