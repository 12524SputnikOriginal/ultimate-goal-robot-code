package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.modules.Shooter;

@TeleOp
@Disabled
public class testShooter extends LinearOpMode {
    Shooter testAuto;
    public void runOpMode() {
        Shooter testAuto = new Shooter(this);
        telemetry.addLine("hachu chislo");
        telemetry.update();
        waitForStart();
        while(opModeIsActive()) {
            testAuto.setAngle(0.57);
            if (gamepad1.a){
                testAuto.shoot();
            }
            if (gamepad1.y){
                testAuto.shoot();
                testAuto.shoot();
                testAuto.shoot();
                testAuto.shoot();
            }
            if (gamepad1.b){
                testAuto.stopShooter();
            }
            telemetry.addData("angular velocity:", testAuto.getAngularVelocity());
            telemetry.update();
            sleep(100);
        }
    }
}
