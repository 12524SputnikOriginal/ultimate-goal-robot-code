package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.DcMotor.RunMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

@Autonomous
@Disabled
public class Goal extends LinearOpMode {
    public void runOpMode(){
        DcMotor m2=hardwareMap.dcMotor.get("m2");
        Servo n3=hardwareMap.servo.get("n3");
        Servo n5=hardwareMap.servo.get("n5");
        waitForStart();
        n3.setPosition(0.3);
        n5.setPosition(0.44);
        m2.setPower(-0.7);
        sleep(4000);
        n3.setPosition(0.6);
        sleep(300);
        n3.setPosition(0.3);
        sleep(300);
        n3.setPosition(0.6);
        sleep(300);
        n3.setPosition(0.3);
        sleep(500);
        n3.setPosition(0.6);
        sleep(300);
        n3.setPosition(0.3);
        sleep(300);
        m2.setPower(0);
    }
}
