package org.firstinspires.ftc.teamcode.auto;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.auto.routes.encoders.BlueMidAEnc;
import org.firstinspires.ftc.teamcode.auto.routes.sensors.RedMidA;
import org.firstinspires.ftc.teamcode.auto.routes.sensors.RedMidB;
import org.firstinspires.ftc.teamcode.auto.routes.sensors.RedMidC;
import org.firstinspires.ftc.teamcode.auto.routes.sensors.RedSideA;
import org.firstinspires.ftc.teamcode.auto.routes.sensors.RedSideB;
import org.firstinspires.ftc.teamcode.auto.routes.sensors.RedSideC;

import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.FOUR;
import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.ONE;
import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.ZERO;
@Autonomous
public class RedMidSensors extends AutoParent {
    @Override
    public void runOpMode() {
        initer(this);
        waitForStart();
        AbstractPath way=new BlueMidAEnc();
        if (recognition.getAnalysis()==ZERO) {
            way=new RedSideA();
            telemetry.addLine("zone A");
            telemetry.update();
        }
        if (recognition.getAnalysis()==ONE){
            way=new RedSideB();
            telemetry.addLine("zone B");
            telemetry.update();
        }
        if (recognition.getAnalysis()==FOUR) {
            way=new RedSideC();
            telemetry.addLine("zone C");
            telemetry.update();
        }
        wob.wobbleIn();
        sleep(400);
        wob.move(-0.8,900);
        dt.move(0, 1, 0, 1600);
        // dt.setPower(0,0,0);
        dt.setPower(0.7, 0, -0.1);
        sleep(2500);
        dt.setPower(0, 0.35, 0);
        dt.waitForColoredLine();
        dt.move(0, -0.4, 0,145);
        dt.setPower(0.5, 0, 0);
        sleep(600);
        dt.move(0, 0, 0.5, 100);
        wob.move(0.8,900);
        sh.setAngle(-0.05);
        sh.setAngularVelocity();
        sh.shoot();
        sh.setAngle(sh.getAngle()-0.01);
        sh.shoot();
        sh.setAngle(sh.getAngle()-0.01);
        telemetry.addData("shooter power", sh.shooter.getPower());
        telemetry.update();
        sh.shoot();
        sh.stopShooter();
        dt.setPower(0.5, 0, 0);
        sleep(500);
        wob.move(-0.6,500);
        wob.wobbleStop();
        dt.move(0,0,-0.1,40);
        way.startPath(dt, wob, sh);
        if (way.getClass()== RedSideB.class){
            return;
        }
        dt.move(0, -0.7, 0, 1500);
        dt.setPower(0,-0.4,0);
        sleep(800);
        //dt.move(0,0.5,0,100);
        dt.setPower(-0.5,0,-0.1);
        dt.waitForColoredLine();
        dt.move(0.5,0,0,90);
        wob.move(0.8,400);
        dt.move(0,0.7,0,50);
        wob.wobbleIn();
        sleep(400);
        wob.move(-0.8,500);
        dt.setPower(0,-0.5,0);
        sleep(600);
        dt.setPower(0.5,0,0);
        sleep(2000);
        dt.move(0, 1, 0, 1500);
        dt.setPower(0, 0.35, 0);
        dt.waitForColoredLine();
        dt.move(0, -0.4, 0,145);
        dt.setPower(0.5, 0, 0);
        sleep(600);
        way.startPath(dt, wob, sh);
        dt.move(0,0,0.3,300);
        dt.move(0,0.8,0,300);
        //*wob.wobbleIn();
        //sleep(400);
        //wob.move(-0.8,800);
        //dt.setPower(0, 0.4, 0);
        //dt.waitForWhiteLine();
        //dt.move(0, -0.4, 0,500);
        //wob.move(0.6,800);
        //sleep(300);
        //wob.wobbleDown();
        /*sh.setAngle(0.56);
        sh.setAngularVelocity();
        sleep(4000);
        dt.move(0, 0, -0.3, 100);
        sh.shoot();
        dt.move(0, 0, 0.3, 45);
        sh.shoot();
        dt.move(0, 0, 0.3, 40);
        sh.shoot();
        sh.stopShooter();
        wob.move(-0.8, 500);
        //dt.move(0, 0, -0.3, 50);
        dt.move(0, -0.5, 0, 100);
        way.startPath(dt,wob,sh);
        */

    }
}