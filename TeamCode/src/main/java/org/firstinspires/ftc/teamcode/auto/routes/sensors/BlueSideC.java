package org.firstinspires.ftc.teamcode.auto.routes.sensors;

import org.firstinspires.ftc.teamcode.auto.AbstractPath;
import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;

public class BlueSideC extends AbstractPath {
    @Override
    public void startPath(DriveTrain dt, Wobble wob, Shooter sh){
        DriveTrain mover = dt;
        setDt(dt);
        Wobble wobble  = wob;
        Shooter shooter = sh;
        wobble.wobbleIn();
        sleep(500);
        mover.setPower(0,0.3,0);
        mover.waitForWhiteLine();
        sleep(300);
        mover.waitForColoredLine();
        sleep(300);
        mover.waitForColoredLine();
        sleep(300);
        mover.move(0, -0.3, 0, 50);
        sleep(300);
        mover.setPower(0,0,0);
        wobble.wobbleOut();
        sleep(500);
        mover.setPower(0,-0.4,0);
        sleep(300);
        mover.waitForWhiteLine();
        mover.move(0, 0.4, 0, 70);
        mover.setPower(0,0,0);
        sleep(60);
        wobble.wobbleMid();
        sleep(1000);
    }
}
