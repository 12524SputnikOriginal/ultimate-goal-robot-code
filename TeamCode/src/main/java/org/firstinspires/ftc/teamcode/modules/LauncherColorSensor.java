package org.firstinspires.ftc.teamcode.modules;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.modules.recognition.SputnikColorSensor;

@TeleOp
public class LauncherColorSensor extends LinearOpMode {
    SputnikColorSensor colorSensor;
    public void runOpMode() {
        colorSensor= new SputnikColorSensor();
        colorSensor.init(hardwareMap);
        waitForStart();
        while (opModeIsActive()) {
            telemetry.addData("White", colorSensor.getWhiteLine());
            telemetry.addData("Black, Red, Blue", colorSensor.getCoroledLine());
            telemetry.update();
        }
    }
}
