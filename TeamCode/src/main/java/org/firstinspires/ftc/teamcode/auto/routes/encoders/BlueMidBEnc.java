package org.firstinspires.ftc.teamcode.auto.routes.encoders;

import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;
import org.firstinspires.ftc.teamcode.auto.AbstractPath;

public class BlueMidBEnc extends AbstractPath {
    @Override
    public void startPath(DriveTrain dt, Wobble wob, Shooter sh)
    {
        DriveTrain mover = dt;
        setDt(dt);
        Wobble wobble = wob;
        wobble.move(-0.6,500);
        wobble.wobbleStop();
        sleep(500);
        mover.move(0, 0.7, 0, 550);
        mover.move(0, 0, -0.5, 530);
        sleep(500);
        wobble.wobbleOut();
        sleep(500);
        mover.move(0, -0.7, 0, 100);
        sleep(100);
        mover.move(0, 0, -0.5, 530);
        mover.move(0, -0.7, 0, 500);
    }
}