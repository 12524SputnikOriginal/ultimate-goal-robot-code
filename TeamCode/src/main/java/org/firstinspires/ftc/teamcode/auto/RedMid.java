package org.firstinspires.ftc.teamcode.auto;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.auto.routes.encoders.BlueSideAEnc;
import org.firstinspires.ftc.teamcode.auto.routes.encoders.RedMidAEnc;
import org.firstinspires.ftc.teamcode.auto.routes.encoders.RedMidBEnc;
import org.firstinspires.ftc.teamcode.auto.routes.encoders.RedMidCEnc;

import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.FOUR;
import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.ONE;
import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.ZERO;
@Autonomous
public class RedMid extends AutoParent {
    @Override
    public void runOpMode() {
        initer(this);
        waitForStart();
        AbstractPath way=new RedMidAEnc();
        if (recognition.getAnalysis()==ZERO) {
            way=new BlueSideAEnc();
            telemetry.addLine("zone A");
            telemetry.update();
        }
        if (recognition.getAnalysis()==ONE){
            way=new RedMidBEnc();
            telemetry.addLine("zone B");
            telemetry.update();
        }
        if (recognition.getAnalysis()==FOUR) {
            way = new RedMidCEnc();
            telemetry.addLine("zone C");
            telemetry.update();
        }
        wob.wobbleIn();
        wob.move(-0.6, 500);
        wob.wobbleStop();
        dt.move(0, 0.7, 0, 1300);
        wob.move(0.6, 500);
        wob.wobbleStop();
        dt.move(0, 0, -0.3, 350);
        sh.setAngle(0.57);
        sh.shoot();
        dt.move(0, 0, -0.5, 50);
        sh.shoot();
        dt.move(0, 0, -0.5, 50);
        sh.shoot();
        dt.move(0, 0, 0.3, 400);
        way.startPath(dt,wob,sh);
    }
}
