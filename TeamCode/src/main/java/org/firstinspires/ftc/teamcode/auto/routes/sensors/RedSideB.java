package org.firstinspires.ftc.teamcode.auto.routes.sensors;

import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;
import org.firstinspires.ftc.teamcode.auto.AbstractPath;

public class RedSideB extends AbstractPath {
    @Override
    public void startPath(DriveTrain dt, Wobble wob, Shooter sh){
        DriveTrain mover = dt;
        setDt(dt);
        Wobble wobble  = wob;
        Shooter shooter = sh;
        wobble.wobbleIn();
        sleep(400);
        //mover.setPower(0.4, 0, 0);
        //sleep(700);
        mover.move(0, 0, 0.56,300);
        mover.move(0, 0.8, 0,1200);
        sleep(400);
        wobble.wobbleOut();
        sleep(400);
        mover.move(0, -0.8, 0,1700);
        mover.move(0.4,0,0,1000);
        //mover.setPower(0.4, 0, 0);
        //sleep(700);
        mover.move(0,-1,0,1500);
        //mover.setPower(0.5,0,0);
        //sleep(2000);
        mover.setPower(0,-0.4,0);
        sleep(400);
        mover.setPower(0,0,0.5);
        sleep(1000);
        mover.setPower(-0.5,0,0);
        sleep(500);
        mover.setPower(0, 0.35, 0);
        mover.waitForColoredLine();
        sleep(300);
        mover.waitForColoredLine();
        mover.move(0, -0.5, 0,300);
        mover.move(0, 0, -0.56,400);
        mover.move(0,0.7,0,200);
        wobble.wobbleIn();
        sleep(400);
        wobble.lift.setTargetPosition(-800);
        wobble.lift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        wobble.lift.setPower(-0.5);
        mover.move(0, 0, -0.3,145);
        mover.setPower(0.5,0,0);
        sleep(2000);
        mover.move(0,1,0,1400);
        mover.setPower(0,0.35,0);
        mover.waitForColoredLine();
        mover.setPower(0.4, 0, 0);
        sleep(600);
        mover.move(0, 0, 0.56,400);
        mover.move(0, 0.8, 0,600);
        wobble.wobbleOut();
        sleep(400);
        mover.move(0, -0.8, 0,200);


        /*mover.setPower(0, 0.35, 0);
        mover.waitForWhiteLine();
        mover.waitForColoredLine();
        mover.move(0,0.7,0,800);
        mover.move(-0.5,0,0,270);
        mover.move(0, 0, 0.5, 540);
        mover.setPower(0, -0.4, 0);
        sleep(900);
        mover.setPower(0,0.35,0);
        mover.waitForColoredLine();
        mover.move(0, 0.4, 0, 50);
        wobble.wobbleOut();
        sleep(400);
        mover.setPower(0, -0.4, 0);
        mover.waitForColoredLine();
        mover.move(0, -0.7, 0, 350);//противовес
        mover.move(0, 0, -0.5, 500);
        mover.setPower(0.4,0,0);
        sleep(800);
        mover.setPower(0, -0.35, 0);
        mover.waitForWhiteLine();
        mover.move(0, 0.7, 0,70);
        wobble.wobbleMid();
        sleep(1000);
        */

    }
}