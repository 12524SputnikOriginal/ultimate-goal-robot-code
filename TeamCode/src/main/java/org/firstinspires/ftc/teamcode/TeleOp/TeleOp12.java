package org.firstinspires.ftc.teamcode.TeleOp;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Intake;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;


@TeleOp
public class TeleOp12 extends LinearOpMode {
        DriveTrain driveTrain;
        Shooter shooter;
    public void runOpMode() {
        DriveTrain driveTrain = new DriveTrain(this);
        Shooter shooter = new Shooter(this);
        Intake intake = new Intake(this);
        Wobble wobble = new Wobble(this);
        telemetry.addLine("I m active");
        int wobbleLift = -10;
        float rotate = 0;
        double PowerCoeF = 1;
        boolean wobbleSwitch = false;
        boolean gamepad2RightTriggerFlag = false;
        boolean gamepad2LeftTriggerFlag = false;
        boolean gamepad2RightBumperFlag = false;
        boolean gamepad2DpadDownFlag = false;
        boolean gamepad2DpadUpFlag = false;
        boolean gamepad2BFlag = false;
        boolean shooterSwitch = false;
        boolean PowerFlag = false;
        int position = 1;
        DigitalChannel limitSwitch = hardwareMap.digitalChannel.get("limitSwitch");
        waitForStart();

        while (opModeIsActive()) {
            if (gamepad1.y && !PowerFlag) {
                if (PowerCoeF == 1) {
                    PowerCoeF = 0.55;
                } else PowerCoeF = 1;
            }
            PowerFlag = gamepad1.y;
            if (gamepad1.left_stick_y != 0 || gamepad1.left_stick_x != 0 || gamepad1.left_trigger != 0 || gamepad1.right_trigger != 0) {
                if (gamepad1.left_trigger != 0) {
                    rotate = gamepad1.left_trigger;
                } else {
                    rotate = -gamepad1.right_trigger;
                }
                driveTrain.setPower(gamepad1.left_stick_x * PowerCoeF, -gamepad1.left_stick_y * PowerCoeF, rotate * PowerCoeF);
            } else {
                driveTrain.setPower(0, 0, 0);
            }
            if (gamepad2.right_trigger != 0 && !gamepad2RightTriggerFlag && position < 3) {
                position = position + 1;
            }
            if (gamepad2.left_trigger != 0 && !gamepad2LeftTriggerFlag && position > 1) {
                position = position - 1;
            }
            if (position == 1) {
                wobble.lift.setTargetPosition(0);
                wobble.lift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
                wobble.lift.setPower(0.5);
            }
            if (position == 2) {
                wobble.lift.setTargetPosition(-1100);
                wobble.lift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
                wobble.lift.setPower(-0.5);
            }
            if (position == 3) {
                wobble.lift.setTargetPosition(-2350);
                wobble.lift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
                wobble.lift.setPower(-0.5);
            }
             /*if(limitSwitch.getState()==false && position==1){
                wobble.lift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            }*/
            if (gamepad1.x) {
                wobble.wobbleOut();
                telemetry.addLine("I m free");
                telemetry.update();
            }
            if (gamepad1.b) {
                wobble.wobbleIn();
                telemetry.addLine("I m first");
                telemetry.update();
            }
            if (gamepad1.a) {
                shooter.setAngle(0.685);
                wobble.wobbleKrivo();
            }
            if (gamepad2.a) {
                shooter.shooterPush.setPosition(0.8);
                sleep(200);
                shooter.shooterPush.setPosition(0.28);
            }
            if (gamepad2.right_bumper && !gamepad2RightBumperFlag) {
                if (shooter.shooter.getPower() == 0) {
                    shooterSwitch = true;
                } else {
                    shooter.shooter.setPower(0);
                    shooterSwitch = false;
                }
            } else {
                if (gamepad2.left_bumper) {
                    shooter.setLowAngularVelocity();
                }
            }
            if (shooterSwitch) {
                shooter.setAngularVelocity();
            } else {
                if (!gamepad2.left_bumper) {
                    shooter.shooter.setPower(0);
                }
            }
            if (gamepad2.right_stick_y > 0) {
                intake.intake.setPower(1);
            } else {
                if (gamepad2.right_stick_y < 0) {
                    intake.intake.setPower(-1);
                } else intake.stop();
            }
            if (gamepad2.dpad_up && shooter.shooterAngleLeft.getPosition() < 0.54 && !gamepad2DpadUpFlag) {
                shooter.shooterAngleLeft.setPosition(shooter.shooterAngleLeft.getPosition() + 0.01);
                shooter.shooterAngleRight.setPosition(shooter.shooterAngleRight.getPosition() + 0.01);
                sleep(50);
            }

            if (gamepad2.dpad_down && shooter.shooterAngleLeft.getPosition() > 0.35 && !gamepad2DpadDownFlag) {
                shooter.shooterAngleLeft.setPosition(shooter.shooterAngleLeft.getPosition() - 0.01);
                shooter.shooterAngleRight.setPosition(shooter.shooterAngleRight.getPosition() - 0.01);
                sleep(50);
            }
            if (gamepad2.dpad_right) {
                shooter.shooterAngleLeft.setPosition(0.46);
                shooter.shooterAngleRight.setPosition(0.46);
            }
            if (gamepad2.dpad_left) {
                shooter.setAngle(0);
            }
            if (gamepad2.y) {
                shooter.shooterAngleLeft.setPosition(0.25);
                shooter.shooterAngleRight.setPosition(0.25);
                intake.intake();
            } else {
                if (gamepad2.x) {
                    shooter.shooterAngleLeft.setPosition(0.25);
                    shooter.shooterAngleRight.setPosition(0.25);
                    intake.throwOut();
                } else {
                    intake.stop();
                }
            }
            if (gamepad2.b && !gamepad2BFlag) {
                //intake.captureOpenerRight.setPower(-0.3);
                shooter.shoot();
                shooter.shoot();
                shooter.shoot();
                shooter.shoot();
            } else {
                //intake.captureOpenerRight.setPower(0);
            }
            gamepad2BFlag = gamepad2.b;
            if (gamepad1.right_bumper) {
                driveTrain.move(0, 0, -0.5, 50);
            }
            if (gamepad1.left_bumper) {
                driveTrain.move(0, 0, 0.5, 50);
            }
            if (gamepad1.left_bumper) {
                driveTrain.move(0, 0, 0.5, 50);
            }
            if (gamepad1.right_bumper) {
                driveTrain.move(0, 0, -0.5, 50);
            }

            if(gamepad1.dpad_right){
                shoot3Rings();
            }
            gamepad2RightBumperFlag = gamepad2.right_bumper;
            gamepad2DpadUpFlag = gamepad2.dpad_up;
            gamepad2DpadDownFlag = gamepad2.dpad_down;
            gamepad2BFlag = gamepad2.b;
            gamepad2RightTriggerFlag = gamepad2.right_trigger != 0;
            gamepad2LeftTriggerFlag = gamepad2.left_trigger != 0;
            //telemetry.addData("right", gamepad2RightTriggerFlag);
            //telemetry.addData("left", gamepad2LeftTriggerFlag);
            //telemetry.addData("k1", wobbleSwitch);
            //telemetry.addData("k2", limitSwitch.getState());
            telemetry.addData("coefficient",shooter.shooter.getPIDFCoefficients(DcMotor.RunMode.RUN_USING_ENCODER));
            telemetry.addData("v", wobbleLift);
            //telemetry.addData("W", wobble.lift.getCurrentPosition());
            telemetry.addData("m1", intake.ringlift.getCurrentPosition());
            telemetry.addData("shooterAngle", shooter.getAngle());
            telemetry.addData("lift", wobble.lift.getTargetPosition());
            telemetry.addData("position", position);
            telemetry.addData("shooterPower", shooter.getAngularVelocity());
            //telemetry.addData("h4",n4.getPosition());
            telemetry.update();
            sleep(5);
        }
    }
        public void shoot3Rings(){
            driveTrain.setPower(0, 0.4, 0);
            driveTrain.waitForColoredLine();
            if(gamepad2.left_stick_button){
                return;
            }
            driveTrain.move(0, -0.4, 0,145);
            driveTrain.setPower(0.5, 0, 0);
            sleep(600);
            if(gamepad2.left_stick_button){
                return;
            }
            driveTrain.move(0, 0, 0.5, 85);
            shooter.setAngle(-0.07);
            if(gamepad2.left_stick_button){
                return;
            }
            shooter.setAngularVelocity();
            if(gamepad2.left_stick_button){
                return;
            }
            shooter.shoot();
            if(gamepad2.left_stick_button){
                return;
            }
            shooter.setAngle(shooter.getAngle()-0.01);
            shooter.shoot();
            if(gamepad2.left_stick_button){
                return;
            }
            shooter.setAngle(shooter.getAngle()-0.01);
            shooter.shoot();
            if(gamepad2.left_stick_button){
                return;
            }
            shooter.setAngle(shooter.getAngle()-0.01);
            shooter.shoot();
            shooter.stopShooter();
    }
}
