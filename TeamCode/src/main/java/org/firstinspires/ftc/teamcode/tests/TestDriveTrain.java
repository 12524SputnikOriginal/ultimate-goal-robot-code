package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.modules.DriveTrain;

@Autonomous
@Disabled
public class TestDriveTrain extends LinearOpMode {
    public void runOpMode(){
        DriveTrain base = new DriveTrain(this);
        waitForStart();
        base.move(0, 0 ,0.5 ,465);
        sleep(100);
    }
}
