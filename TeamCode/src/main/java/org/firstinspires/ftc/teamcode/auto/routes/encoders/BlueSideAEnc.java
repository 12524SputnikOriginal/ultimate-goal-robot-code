package org.firstinspires.ftc.teamcode.auto.routes.encoders;

import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;
import org.firstinspires.ftc.teamcode.auto.AbstractPath;

public class BlueSideAEnc extends AbstractPath {
    @Override
    public void startPath(DriveTrain dt, Wobble wob, Shooter sh)
    {
        DriveTrain mover = dt;
        setDt(dt);
        Wobble wobble = wob;
        wobble.wobbleIn();
        sleep(1000);
        wobble.move(-0.6, 500);
        wobble.wobbleStop();
        mover.setPower(-0.5, 0, 0);
        sleep(1500);
        mover.move(0, 0.7, 0, 2400);
        sleep(100);
        mover.setPower(-0.5, 0, 0);
        sleep(1000);
        wobble.wobbleOut();
        sleep(500);
        mover.move(0, -0.7, 0, 0);
        wobble.wobbleIn();
        sleep(500);
    }
}
