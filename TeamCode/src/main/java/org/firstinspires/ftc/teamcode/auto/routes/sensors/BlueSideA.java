package org.firstinspires.ftc.teamcode.auto.routes.sensors;

import org.firstinspires.ftc.teamcode.auto.AbstractPath;
import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;

public class BlueSideA extends AbstractPath {
    @Override
    public void startPath(DriveTrain dt, Wobble wob, Shooter sh)
    {
        DriveTrain mover = dt;
        setDt(dt);
        Wobble wobble = wob;
        wobble.wobbleIn();
        sleep(500);
        mover.setPower(-0.4, 0, 0);
        sleep(500);
        mover.setPower(0, 0.35, 0);
        mover.waitForWhiteLine();
        mover.move(0, -0.4, 0, 60);
        sleep(100);
        wobble.wobbleDown();
        sleep(500);
        wobble.wobbleStop();
        wobble.wobbleOut();
        sleep(500);
        mover.move(0,-0.2,0,50);
        wobble.wobbleMid();
        sleep(600);
    }
}
