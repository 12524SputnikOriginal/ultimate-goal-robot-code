package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.recognition.SputnikColorSensor;

@Autonomous
@Disabled
public class TestGetHighSpeedLine extends LinearOpMode {
    SputnikColorSensor colorSensor;
    public void runOpMode(){
        DriveTrain base = new DriveTrain(this);
        waitForStart();
        DriveTrain mover = base;
        mover.setPower(0,0.4,0);
        mover.waitForHighSpeedLine();
        mover.move(0,-0.4,0,70);
    }
}
