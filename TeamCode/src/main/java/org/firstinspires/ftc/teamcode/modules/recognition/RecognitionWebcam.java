package org.firstinspires.ftc.teamcode.modules.recognition;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.openftc.easyopencv.OpenCvCamera;
import org.openftc.easyopencv.OpenCvCameraFactory;
import org.openftc.easyopencv.OpenCvCameraRotation;

@TeleOp
public class RecognitionWebcam extends LinearOpMode {
    OpenCvCamera webcam;
    RingRecognition pipeline;
    public void runOpMode(){
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        webcam = OpenCvCameraFactory.getInstance().createWebcam(hardwareMap.get(WebcamName.class, "Webcam 1"), cameraMonitorViewId);
        pipeline = new RingRecognition();
        webcam.openCameraDevice();
        webcam.setPipeline(pipeline);
        webcam.startStreaming(320,240,OpenCvCameraRotation.UPRIGHT);
        boolean gamepad1DpadLeftFlag=false;
        boolean gamepad1DpadRightFlag=false;
        boolean gamepad1DpadUpFlag=false;
        boolean gamepad1DpadDownFlag=false;
        boolean gamepad1AFlag=false;
        boolean gamepad1BFlag=false;
        boolean gamepad1XFlag=false;
        boolean gamepad1YFlag=false;
        while(!isStopRequested()){
            sleep(50);
            if(!gamepad1DpadLeftFlag && gamepad1.dpad_left ){
                pipeline.a+=5;
            }
            gamepad1DpadLeftFlag=gamepad1.dpad_left;
            if( gamepad1DpadRightFlag && gamepad1.dpad_right){
                pipeline.a-=5;
            }
            gamepad1DpadRightFlag=gamepad1.dpad_right;
            if (!gamepad1DpadUpFlag && gamepad1.dpad_up){
                pipeline.b+=5;
                pipeline.c+=5;
            }
            gamepad1DpadUpFlag=gamepad1.dpad_up;
            if (!gamepad1DpadDownFlag && gamepad1.dpad_down){
                pipeline.b-=5;
                pipeline.c-=5;
            }
            gamepad1DpadDownFlag=gamepad1.dpad_down;
            if (!gamepad1AFlag&& gamepad1.a){
                pipeline.maxval+=2;
            }
            gamepad1AFlag=gamepad1.a;
            if (!gamepad1BFlag&& gamepad1.b){
                pipeline.maxval-=2;
            }
            gamepad1BFlag=gamepad1.b;
            if (!gamepad1XFlag&& gamepad1.x){
                pipeline.thresh+=2;
            }
            gamepad1XFlag=gamepad1.x;
            if (!gamepad1YFlag&& gamepad1.y){
                pipeline.thresh-=2;
            }
            gamepad1YFlag=gamepad1.y;
            telemetry.addData("position is ", pipeline.getAnalysis());
            telemetry.addData("avg1 is ", pipeline.getAvgs()[0]);
            telemetry.addData("avg 2 is", pipeline.getAvgs()[1]);
            telemetry.addData("black avg is", pipeline.getAvgs()[2]);
            telemetry.addData("shiftRightLeft",pipeline.a);
            telemetry.addData("shiftUpDowm",pipeline.b);
            telemetry.addData("thresh",pipeline.thresh);
            telemetry.addData("maxval",pipeline.maxval);
            telemetry.update();
        }
    }

}

