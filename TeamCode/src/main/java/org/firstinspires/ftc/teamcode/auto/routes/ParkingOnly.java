package org.firstinspires.ftc.teamcode.auto.routes;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.auto.AutoParent;

@Autonomous
public class ParkingOnly extends AutoParent {
    @Override
    public void runOpMode(){
        initer(this);
        waitForStart();
        sleep(25000);
        //dt.move(0, -0.8, 0, 100);
        //sh.setAngularVelocity();
        //sleep(10000);
        //sh.setAngle(0.38);
        //sh.shoot();
        //sleep(2000);
        //dt.move(0, 0, 0.5, 40);
        //sh.shoot();
        //sleep(2000);
        //dt.move(0, 0, 0.5, 40);
        //sh.shoot();
        //dt.move(0, 0.8, 0, 100);
        dt.move(0, 1, 0, 2500);
        dt.setPower(0, 0.35, 0);
        dt.waitForWhiteLine();
        dt.move(0, -1, 0, 50);
    }
}