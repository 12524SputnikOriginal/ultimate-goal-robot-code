package org.firstinspires.ftc.teamcode.auto;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.auto.routes.encoders.RedSideAEnc;
import org.firstinspires.ftc.teamcode.auto.routes.encoders.RedSideBEnc;
import org.firstinspires.ftc.teamcode.auto.routes.encoders.RedSideCEnc;
import org.firstinspires.ftc.teamcode.auto.routes.sensors.RedSideA;

import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.FOUR;
import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.ONE;
import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.ZERO;
@Autonomous
public class RedSide extends AutoParent {
    @Override
    public void runOpMode(){
        initer(this);
        waitForStart();
        AbstractPath way=new RedSideA();
        if (recognition.getAnalysis()==ZERO) {
            way=new RedSideAEnc();
            telemetry.addLine("zone A");
            telemetry.update();
        }
        if (recognition.getAnalysis()==ONE){
            way=new RedSideBEnc();
            telemetry.addLine("zone B");
            telemetry.update();
        }
        if (recognition.getAnalysis()==FOUR) {
            way = new RedSideCEnc();
            telemetry.addLine("zone C");
            telemetry.update();
        }
        wob.wobbleIn();
        sleep(200);
        wob.move(-0.6, 600);
        dt.setPower(0.5, 0, 0);
        sleep(1000);
        dt.setPower(0,0.4,0);
        dt.waitForColoredLine();
        dt.move(0, -0.4, 0, 70);
        wob.move(0.6, 500);
        wob.wobbleStop();
        dt.setPower(0.5, 0, 0);
        sleep(300);
        dt.move(0, 0, 0.5, 75);
        sleep(1000);
        sh.setAngle(0.62);
        sh.shoot();
        sh.shoot();
        telemetry.addData("shooter power", sh.shooter.getPower());
        telemetry.update();
        sh.shoot();
        sh.stopShooter();
        dt.setPower(0.5, 0, 0);
        sleep(300);
        dt.setPower(0, 0, 0);
        sleep(300);
        way.startPath(dt,wob,sh);
    }
}