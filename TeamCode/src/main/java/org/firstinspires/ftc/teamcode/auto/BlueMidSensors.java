package org.firstinspires.ftc.teamcode.auto;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.auto.routes.encoders.BlueMidAEnc;
import org.firstinspires.ftc.teamcode.auto.routes.sensors.BlueMidA;
import org.firstinspires.ftc.teamcode.auto.routes.sensors.BlueMidB;
import org.firstinspires.ftc.teamcode.auto.routes.sensors.BlueMidC;

import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.FOUR;
import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.ONE;
import static org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition.RingPosition.ZERO;
@Autonomous
public class BlueMidSensors extends AutoParent {
    @Override
    public void runOpMode() {
        initer(this);
        waitForStart();
        AbstractPath way = new BlueMidAEnc();
        if (recognition.getAnalysis() == ZERO) {
            way = new BlueMidA();
            telemetry.addLine("zone A");
            telemetry.update();
        }
        if (recognition.getAnalysis() == ONE) {
            way = new BlueMidB();
            telemetry.addLine("zone B");
            telemetry.update();
        }
        if (recognition.getAnalysis() == FOUR) {
            way = new BlueMidC();
            telemetry.addLine("zone C");
            telemetry.update();
        }
        wob.wobbleIn();
        sleep(500);
        wob.move(-0.8, 800);
        dt.setPower(0, 0.35, 0);
        dt.waitForWhiteLine();
        dt.move(0, -0.4, 0,500);
        wob.move(0.8, 800);
        sleep(300);
        wob.wobbleDown();
        wob.wobbleStop();
        sh.setAngle(0.58);
        dt.move(0, 0, -0.3, 100);
        sh.setAngularVelocity();
        sleep(3000);
        sh.shoot();
        dt.move(0, 0, -0.3, 50);
        sh.shoot();
        dt.move(0, 0, -0.3, 60);
        sh.shoot();
        sh.stopShooter();
        dt.move(0, 0, 0.3, 200);
        way.startPath(dt, wob, sh);
        telemetry.addData("shoterPush",sh.shooterPush);
        telemetry.update();
    }
}

