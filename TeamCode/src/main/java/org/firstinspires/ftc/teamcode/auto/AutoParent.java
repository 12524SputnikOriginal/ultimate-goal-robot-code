package org.firstinspires.ftc.teamcode.auto;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.recognition.RingRecognition;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;
import org.openftc.easyopencv.OpenCvCamera;
import org.openftc.easyopencv.OpenCvCameraFactory;
import org.openftc.easyopencv.OpenCvCameraRotation;

public abstract class AutoParent extends LinearOpMode {
    OpenCvCamera webcam;
    protected DriveTrain dt;
    protected Wobble wob;
    protected Shooter sh;
    protected RingRecognition recognition;
    public void initer(LinearOpMode opMode){
        dt=new DriveTrain(opMode);
        wob=new Wobble(opMode);
        sh= new Shooter(opMode);
        dt.init(hardwareMap);
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        webcam = OpenCvCameraFactory.getInstance().createWebcam(hardwareMap.get(WebcamName.class, "Webcam 1"), cameraMonitorViewId);
        recognition = new RingRecognition();
        webcam.openCameraDevice();
        webcam.setPipeline(recognition);
        webcam.startStreaming(320,240, OpenCvCameraRotation.UPRIGHT);
        recognition.getAnalysis();
        boolean gamepad1DpadLeftFlag=false;
        boolean gamepad1DpadRightFlag=false;
        boolean gamepad1DpadUpFlag=false;
        boolean gamepad1DpadDownFlag=false;
        boolean gamepad1XFlag=false;
        boolean gamepad1YFlag=false;
        boolean gamepad1AFlag=false;
        boolean gamepad1BFlag=false;
        while(!isStarted()){
            sleep(50);
            if(!gamepad1DpadLeftFlag && gamepad1.dpad_left ){
                recognition.a+=5;
            }
            gamepad1DpadLeftFlag=gamepad1.dpad_left;
            if( gamepad1DpadRightFlag && gamepad1.dpad_right){
                recognition.a-=5;
            }
            gamepad1DpadRightFlag=gamepad1.dpad_right;
            if (!gamepad1DpadUpFlag && gamepad1.dpad_up){
                recognition.b+=5;
                recognition.c+=5;
            }
            gamepad1DpadUpFlag=gamepad1.dpad_up;
            if (!gamepad1DpadDownFlag && gamepad1.dpad_down){
                recognition.b-=5;
                recognition.c-=5;
            }
            gamepad1DpadDownFlag=gamepad1.dpad_down;
            if (!gamepad1XFlag&& gamepad1.x){
                recognition.thresh+=2;
            }
            gamepad1XFlag=gamepad1.x;
            if (!gamepad1YFlag&& gamepad1.y){
                recognition.thresh-=2;
            }
            gamepad1YFlag=gamepad1.y;
            if (!gamepad1AFlag&& gamepad1.a){
                recognition.maxval+=2;
            }
            gamepad1AFlag=gamepad1.a;
            if (!gamepad1BFlag&& gamepad1.b){
                recognition.maxval-=2;
            }
            gamepad1BFlag=gamepad1.b;
            telemetry.addData("position is ", recognition.getAnalysis());
            telemetry.addData("avg1 is ", recognition.getAvgs()[0]);
            telemetry.addData("avg 2 is", recognition.getAvgs()[1]);
            telemetry.addData("black avg is", recognition.getAvgs()[2]);
            telemetry.addData("shiftRightLeft",recognition.a);
            telemetry.addData("shiftUpDowm",recognition.b);
            telemetry.addData("thresh",recognition.thresh);
            telemetry.addData("maxval",recognition.maxval);
            telemetry.update();
        }
    }
}
