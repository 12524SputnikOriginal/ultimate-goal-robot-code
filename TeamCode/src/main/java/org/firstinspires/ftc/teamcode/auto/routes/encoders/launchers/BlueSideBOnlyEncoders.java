package org.firstinspires.ftc.teamcode.auto.routes.encoders.launchers;

import org.firstinspires.ftc.teamcode.auto.AbstractPath;
import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;

public class BlueSideBOnlyEncoders extends AbstractPath {
    @Override
    public void startPath(DriveTrain dt, Wobble wob, Shooter sh){
        DriveTrain mover = dt;
        Wobble wobble  = wob;
        Shooter shooter = sh;
        wobble.wobbleIn();
        sleep(1000);
        mover.move(-0.5, 0, 0.3, 1000);
        mover.move(0, 0.6, 0, 5000);
        mover.move(0, -0.5, 0, 50);
        sleep(500);
        mover.move(0, 0, -0.5, 600);
        mover.move(0, -0.5, 0, 500);
        sleep(500);
        wobble.wobbleOut();
        mover.move(0, 0.5, 0, 500);
        mover.move(0, 0, 0.5, 600);
        mover.move(0, -0.5, 0, 1000);
        mover.move(0, 0.5, 0, 50);
        wobble.wobbleIn();
        sleep(1000);
    }
}