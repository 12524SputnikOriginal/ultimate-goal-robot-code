package org.firstinspires.ftc.teamcode.auto.routes.encoders;

import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;
import org.firstinspires.ftc.teamcode.auto.AbstractPath;

public class RedSideCEnc extends AbstractPath {
    @Override
    public void startPath(DriveTrain dt, Wobble wob, Shooter sh) {
        DriveTrain mover = dt;
        setDt(dt);
        Wobble wobble = wob;
        wobble.move(-0.6, 500);
        wobble.wobbleStop();
        mover.move(0, 0.6, 0, 2100);
        sleep(500);
        wobble.wobbleOut();
        mover.move(0, -0.6, 0, 1350);
        sleep(500);
        wobble.wobbleMid();
        sleep(1000);
    }
}