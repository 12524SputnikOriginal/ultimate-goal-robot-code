package org.firstinspires.ftc.teamcode.TeleOp;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
@TeleOp
@Disabled
public class ActualTeleOp extends LinearOpMode {
    public void runOpMode() {
        telemetry.addLine("I m active");
        DigitalChannel limitSwitch = hardwareMap.digitalChannel.get("limitSwitch");
        DcMotor p0 = hardwareMap.dcMotor.get("leftForward");
        DcMotor p1 = hardwareMap.dcMotor.get("leftBack");
        DcMotor p2 = hardwareMap.dcMotor.get("rightBack");
        DcMotor p3 = hardwareMap.dcMotor.get("rightForward");
        DcMotor m1 = hardwareMap.dcMotor.get("lift");
        DcMotor m2 = hardwareMap.dcMotor.get("shooter");
        DcMotor m3 = hardwareMap.dcMotor.get("ringlift");
        DcMotor m4 = hardwareMap.dcMotor.get("intake");

        double a, b, c, d;
        Servo leftHand = hardwareMap.servo.get("leftHand");
        Servo rightHand = hardwareMap.servo.get("rightHand");
        Servo n3 = hardwareMap.servo.get("shooterPush");
        Servo n5 = hardwareMap.servo.get("shooterAngleLeft");
        Servo n2 = hardwareMap.servo.get("shooterAngleRight");
        Servo n1 = hardwareMap.servo.get("captureOpenerLeft");
        //Servo n4 = hardwareMap.servo.get("captureOpenerRight");
        //n4.setDirection(Servo.Direction.REVERSE);
        boolean r = false;
        int v = -10;
        double g1 = 1;
        boolean k1 = false;
        boolean h1 = false;
        boolean h2 = false;
        boolean h3 = false;
        boolean h4 = false;
        boolean s1= false;
        m1.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        n5.setPosition(0.48);//положение иницализации угла стрельбы
        n2.setPosition(0.48);//положение иницализации угла стрельбы
        n1.setPosition(0.71);
        sleep(1000);
        n3.setPosition(0.28);
        n2.setDirection(Servo.Direction.REVERSE);

        waitForStart();
        while (opModeIsActive()) {

            if (gamepad1.y && !k1) {
                if (g1 == 1) {
                    g1 = 0.55;
                } else g1 = 1;
            }
            k1 = gamepad1.y;
            telemetry.addData("k1", k1);
            telemetry.addData("g1", g1);
            telemetry.addData("v", v);
            telemetry.addData("m1", m1.getCurrentPosition());
            telemetry.addData("shooterAngle", n5.getPosition());
            //telemetry.addData("h4",n4.getPosition());
            telemetry.update();
            double x = gamepad1.left_stick_x;
            double y = gamepad1.left_stick_y;
            double n = gamepad1.left_trigger;
            double m = gamepad1.right_trigger;
            a = y + x + n - m;
            b = y - x + n - m;
            c = -y - x + n - m;
            d = -y + x + n - m;
            p0.setPower(a * g1);
            p1.setPower(b * g1);
            p2.setPower(c * g1);
            p3.setPower(d * g1);

            if (gamepad2.right_trigger != 0 && m1.getCurrentPosition() - v >= -2500) {
                m1.setPower(-gamepad2.right_trigger);
            }
            else {
                if (gamepad2.left_trigger != 0 && limitSwitch.getState()) {
                    m1.setPower(gamepad2.left_trigger);
                }
                else {
                    m1.setPower(0);
                }
            }
            if (limitSwitch.getState() == false && r == false) {
                v = m1.getCurrentPosition();
                r = true;
            }
            if (limitSwitch.getState() == false && r != false) {
                m1.setPower((m1.getCurrentPosition() - v) * (-0.002));
            }
            if (gamepad1.x) {
                leftHand.setPosition(0.65);
                rightHand.setPosition(0.35);
                telemetry.addLine("I m free");
                telemetry.update();
            }
            if (gamepad1.b) {
                leftHand.setPosition(1);
                rightHand.setPosition(0);
                telemetry.addLine("I m first");
                telemetry.update();
            }
            if (gamepad2.a) {
                n3.setPosition(0.8);
                sleep(200);
                n3.setPosition(0.28);
            }
            if (gamepad2.right_bumper && !h3) {
                if (m2.getPower() == 0) {
                    s1=true;
                }
                else {
                    m2.setPower(0);
                    s1=false;
                }
            }
            else {
                if (gamepad2.left_bumper) {
                    m2.setPower(-0.5);
                }
            }
            if(s1){
                m2.setPower(-0.6 * 13.4 / hardwareMap.voltageSensor.iterator().next().getVoltage());
            }
            else{
                m2.setPower(0);
            }
            h3 = gamepad2.right_bumper;
            if (gamepad2.right_stick_y > 0) {
                m3.setPower(1);
            }
            else {
                if (gamepad2.right_stick_y < 0) {
                    m3.setPower(-1);
                } else m3.setPower(0);
            }
            if (gamepad2.dpad_up && n5.getPosition() < 0.54 && !h1) {
                n5.setPosition(n5.getPosition() + 0.01);
                n2.setPosition(n2.getPosition() + 0.01);
                sleep(50);
            }
            h1 = gamepad2.dpad_up;

            if (gamepad2.dpad_down && n5.getPosition() > 0.35 && !h2) {
                n5.setPosition(n5.getPosition() - 0.01);
                n2.setPosition(n2.getPosition() - 0.01);
                sleep(50);
            }
            h2 = gamepad2.dpad_down;

            if (gamepad2.dpad_right) {
                n5.setPosition(0.46);
                n2.setPosition(0.46);
            }
            if (gamepad2.dpad_left) {
                n5.setPosition(0.43);
                n2.setPosition(0.43);
            }
            if (gamepad2.y) {
                n5.setPosition(0.34);
                n2.setPosition(0.34);
                m4.setPower(1);
                m3.setPower(-1);
            }
            else {
                if (gamepad2.x) {
                    n5.setPosition(0.34);
                    n2.setPosition(0.34);
                    m4.setPower(-1);
                    m3.setPower(1);
                }
                else {
                    m4.setPower(0);
                    m3.setPower(0);
                }
            }
            /*if (gamepad2.b  && !h4) {
                n4.setPosition(n4.getPosition() - 0.05);
                sleep(50);
            }
            h4 = gamepad2.b;*/
        }
    }
}