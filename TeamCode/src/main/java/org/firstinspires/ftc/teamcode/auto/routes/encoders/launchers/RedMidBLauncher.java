package org.firstinspires.ftc.teamcode.auto.routes.encoders.launchers;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.auto.AbstractPath;
import org.firstinspires.ftc.teamcode.auto.routes.encoders.RedMidBEnc;
import org.firstinspires.ftc.teamcode.modules.DriveTrain;
import org.firstinspires.ftc.teamcode.modules.Shooter;
import org.firstinspires.ftc.teamcode.modules.Wobble;

@Autonomous
@Disabled
public class RedMidBLauncher extends LinearOpMode {
    public void runOpMode(){
        DriveTrain dt = new DriveTrain(this);
        Wobble wob = new Wobble();
        AbstractPath route = new RedMidBEnc();
        dt.init(hardwareMap);
        wob.init(hardwareMap);
        waitForStart();
        route.startPath(dt, wob, new Shooter());
    }
}
